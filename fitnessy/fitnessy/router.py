from rest_framework import routers
from fitnessysite.viewsets import (
    TicketViewSet,
    ContactViewSet,
    ServiceViewSet,
    EquipmentViewSet,
    HallViewSet,
    PhotoViewSet)


router = routers.DefaultRouter()
router.register(r'tickets', TicketViewSet)
router.register(r'contacts', ContactViewSet)
router.register(r'halls', HallViewSet)
router.register(r'equipments', EquipmentViewSet)
router.register(r'photos', PhotoViewSet)
router.register(r'services', ServiceViewSet)