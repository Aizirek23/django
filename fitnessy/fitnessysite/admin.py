from django.contrib import admin
from .models import *
from import_export.admin import ExportActionMixin
from simple_history.admin import SimpleHistoryAdmin

# Register your models here.


class EquipmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'count')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title', 'description', 'count')
    list_filter = ('created_at', )


class ContactAdmin(ExportActionMixin, SimpleHistoryAdmin, admin.ModelAdmin):
    list_display = ('id', 'title', 'post', 'tel')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title', 'description', 'post')


class TicketAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'price', 'duration')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title', 'description')


class HallAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'tel', 'isClosed')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title')
    list_filter = ('isClosed',)
    filter_horizontal = ('service',)


class ServiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')
    list_display_links = ('id', 'title')
    search_fields = ('id', 'title', 'description')


class PhotoAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at')
    list_display_links = ('id', 'created_at')
    search_fields = ('id', 'title', 'description')
    list_filter = ('created_at',)


admin.site.register(Equipment, EquipmentAdmin)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Ticket, TicketAdmin)
admin.site.register(Hall, HallAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(Photo, PhotoAdmin)