from django.apps import AppConfig


class FitnessysiteConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fitnessysite'
