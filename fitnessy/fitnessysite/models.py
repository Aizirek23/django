from django.db import models
from simple_history.models import HistoricalRecords

# Create your models here.


class Equipment(models.Model):
    title = models.CharField(max_length=127, verbose_name='Название')
    description = models.CharField(max_length=255, verbose_name='Описание')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата покупки')
    count = models.IntegerField(verbose_name='Количество')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Оборудование'
        verbose_name_plural = 'Оборудование'
        ordering = ['created_at', 'title']


class Contact(models.Model):
    title = models.CharField(max_length=255, verbose_name='ФИО')
    description = models.CharField(max_length=255, verbose_name='Описание')
    post = models.CharField(max_length=255, verbose_name='Должность')
    tel = models.IntegerField(verbose_name='Номер телефона')
    history = HistoricalRecords()
    
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'
        ordering = ['title']


class Ticket(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')
    description = models.CharField(max_length=255, verbose_name='Описание')
    price = models.FloatField(max_length=127, verbose_name='Цена')
    duration = models.IntegerField(verbose_name='Срок действия')
    count = models.IntegerField(verbose_name='Количество посещений')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Тариф'
        verbose_name_plural = 'Тарифы'
        ordering = ['price']


class Hall(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')
    description = models.CharField(max_length=255, verbose_name='Описание')
    address = models.CharField(max_length=255, verbose_name='Адрес')
    tel = models.IntegerField(verbose_name='Номер телефона')
    isClosed = models.BooleanField(default=False, verbose_name='Статус')
    service = models.ManyToManyField('Service', verbose_name='Услуги')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Зал'
        verbose_name_plural = 'Залы'


class Service(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')
    description = models.CharField(max_length=255, verbose_name='Описание')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Улуга'
        verbose_name_plural = 'Услуги'


class Photo(models.Model):
    photo = models.ImageField(verbose_name='Фотография')
    description = models.CharField(max_length=255, verbose_name='Описание')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'