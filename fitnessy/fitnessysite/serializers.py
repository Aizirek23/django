from rest_framework import serializers
from fitnessysite.models import (
    Ticket,
    Equipment,
    Contact,
    Hall,
    Service,
    Photo)

class TicketSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='ticket-detail', read_only=True)
    
    def validate_price(self, value):
        if value > 1000000:
            raise serializers.ValidationError('Слишком большое значение. Оно должно быть меньше 1000000!')
        return value
    
    def validate_count(self, value):
        if value > 100:
            raise serializers.ValidationError('Слишком большое значение. Количество посещений в абонементе не может быть больше 100!')
        return value
    
    class Meta:
        model = Ticket
        fields = ['id', 'title', 'description', 'price', 'count', 'duration', 'url']
        
class SimplifiedTicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields = ['title', 'price']
        
class EquipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Equipment
        fields = '__all__'
        
class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = '__all__'
        
        
class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = '__all__'
        

class HallSerializer(serializers.ModelSerializer):
    service = ServiceSerializer(many=True)
    
    class Meta:
        model = Hall
        fields = ['id', 'title', 'description', 'address', 'tel', 'isClosed', 'service']

        
class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = '__all__'