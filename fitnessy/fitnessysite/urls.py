from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='home'),
    path('tickets', tickets, name='tickets'),
    path('album', album, name='album'),
    path('contacts', contacts, name='contacts'),
    path('halls', halls, name='halls')
]