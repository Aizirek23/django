from django.http import HttpResponseNotFound
from django.shortcuts import render
from .models import *

# Create your views here.


def index(request):

    context = {
        'title': 'О нас'
    }

    return render(request, 'index.html', context=context)


def tickets(request):
    tickets = Ticket.objects.all()

    context = {
        'tickets': tickets,
        'title': 'Список абонементов'
    }

    return render(request, 'tickets.html', context=context)


def album(request):
    photos = Photo.objects.all()

    context = {
        'photos': photos,
        'title': 'Фотоальбом'
    }

    return render(request, 'album.html', context=context)


def contacts(request):
    contacts = Contact.objects.all()

    context = {
        'contacts': contacts,
        'title': 'Список контактов'
    }

    return render(request, 'contacts.html', context=context)


def halls(request):
    halls = Hall.objects.all()

    context = {
        'halls': halls,
        'title': 'Список залов'
    }

    return render(request, 'halls.html', context=context)


def pageNotFound(request, exception):
    return HttpResponseNotFound('Страница не найдена.')