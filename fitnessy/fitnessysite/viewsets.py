from django.db.models import Q
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import filters, generics
from rest_framework.pagination import PageNumberPagination
from fitnessysite.serializers import (
    TicketSerializer,
    SimplifiedTicketSerializer,
    HallSerializer,
    EquipmentSerializer,
    ContactSerializer,
    ServiceSerializer,
    PhotoSerializer) 
from fitnessysite.models import (
    Ticket,
    Hall,
    Service,
    Equipment,
    Contact,
    Photo)

# Q(question__startswith='What')

class MyPaginator(PageNumberPagination):
    page_size = 2
    page_size_query_param = 'page_size'

class TicketViewSet(ModelViewSet):
    queryset = Ticket.objects.all()
    serializer_class = TicketSerializer
    pagination_class = MyPaginator
        
    
    @action(detail=False, methods=['get'], name='Simplified Info', url_path='simple')
    def SimpleView(self, request):
        serializer = SimplifiedTicketSerializer(self.queryset, many=True)
        return Response(serializer.data)
    
    @action(detail=True, methods=['get'], name='Simplified Info', url_path='simple')
    def SimpleObjectView(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = SimplifiedTicketSerializer(instance)
        return Response(serializer.data)
    
class HallViewSet(ModelViewSet):
    queryset = Hall.objects.all()
    serializer_class = HallSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['title', 'service__title']

    
class ContactViewSet(ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    
    def list(self, request, *args, **kwargs):
        queryset = Contact.objects.filter(Q(post='Менеджер') | Q(post='Тренер'))

        if request.user.is_staff:
            queryset = Contact.objects.all()
        
        post = request.GET.get('post', None)
        
        if post != None:
            queryset = Contact.objects.filter(post=post)
        
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
    
class PhotoViewSet(ModelViewSet):
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
    
class EquipmentViewSet(ModelViewSet):
    queryset = Equipment.objects.all()
    serializer_class = EquipmentSerializer
    
class ServiceViewSet(ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer